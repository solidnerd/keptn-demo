package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"os"

	"github.com/Unleash/unleash-client-go"
)

// Search Search
type Search struct {
	Text string
}

type metricsInterface struct {
}

func init() {
	unleash.Initialize(
		unleash.WithUrl(os.Getenv("FF_URL")),
		unleash.WithInstanceId(os.Getenv("FF_INSTANCEID")),
		unleash.WithAppName(os.Getenv("FF_APPNAME")),
		unleash.WithListener(&metricsInterface{}),
	)
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "{\"ping\": \"OK\"}")
}

func home(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: Home")
	if unleash.IsEnabled("my_awsome_new_feature") {
		fmt.Fprintf(w, "<html><body><h1>My new Page!</h1></body></html>")
	} else {
		fmt.Fprintf(w, "<html><body><h1>My old boring Stuff!</h1></body></html>")
	}
}

func error(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte("500 - Something bad happened!"))
}

func search(w http.ResponseWriter, r *http.Request) {
	var s Search

	err := json.NewDecoder(r.Body).Decode(&s)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("500 - Something bad happened!"))
		return
	}
	o := "Search string: " + s.Text
	w.Write([]byte(o))
}

func handleRequests() {
	http.HandleFunc("/ping", ping)
	http.HandleFunc("/error", error)
	http.HandleFunc("/search", error)
	http.HandleFunc("/", home)
	log.Fatal(http.ListenAndServe(":80", nil))
}

func main() {
	handleRequests()
}
